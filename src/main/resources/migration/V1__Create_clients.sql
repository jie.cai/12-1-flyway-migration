create table clients (
    id int auto_increment primary key,
    full_name varchar(128) not null,
    abbreviation varchar(6) not null
);
