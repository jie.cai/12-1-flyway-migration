create table services (
    id int auto_increment primary key,
    type text not null
);

insert into services(type) VALUES ('MOVIE'), ('PARTY'), ('BUSINESS_MEAL');

set @default_service_type_id = (select id from services where type='MOVIE');

alter table contract
    add column service_id int not null,
    add foreign key (service_id) references services(id);

update contract set service_id = @default_service_type_id;
