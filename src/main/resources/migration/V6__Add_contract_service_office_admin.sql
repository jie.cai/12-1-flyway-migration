alter table contract_service_offices
    add column admin_id int,
    add foreign key (admin_id) references staff(id);
