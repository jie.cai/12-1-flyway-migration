create table contract (
    id int auto_increment primary key,
    name text not null,
    client_id int not null,
    foreign key (client_id) references clients(id)
);
