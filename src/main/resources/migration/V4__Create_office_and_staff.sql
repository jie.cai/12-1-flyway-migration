create table offices (
    id int auto_increment primary key,
    country text not null,
    city text not null
);

create table staff (
    id int auto_increment primary key,
    first_name text not null,
    last_name text not null
);
