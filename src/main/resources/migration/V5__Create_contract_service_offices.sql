create table contract_service_offices (
    contract_id int not null,
    office_id int not null,
    PRIMARY KEY (contract_id, office_id),
    foreign key (contract_id) references contract(id),
    foreign key (office_id) references offices(id)
);
